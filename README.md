# Hotsite Combate #

Desenvolvimento do Hotsite do Canal Combate

### Objetivos ###

* Criar interface com boa usabilidade e acessibilidade;
* Mobile first;
* Hotsite de f�cil administra��o, gerenci�vel;

### Metodologias e Tecnologias ###

* Arquitetura Restful;
* API em Python;
* Interface em React;
* Campanhas administradas via DFP;