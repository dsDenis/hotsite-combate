const HtmlWebpackPlugin = require ('html-webpack-plugin');

var HTMLWebpackPluginConfig = new HtmlWebpackPlugin({ template: 'public/index.html' });

module.exports = {
	entry: './app/Index.js',
	output: {
		filename: 'public/bundle.js',
	},
	module: {
		loaders: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				loader: 'babel-loader',
				query: {
					presets: ['react', 'es2015']
				}
			},
			{
				test: /\.scss$/,
				loaders: ['style-loader', 'css-loader', 'sass-loader']
			},
			{
				test: /\.(gif|png|jpe?g|svg|ttf|otf)$/i,
				loader: 'file-loader'
			}
		]
	},
	plugins: [HTMLWebpackPluginConfig]
};

