import React from 'react';

import { Link } from 'react-router-dom';
import ImgBanner from './imagens/destaque-banner.png';

class SliderMain extends React.Component{
	render(){
		return(
			<div className="banner">
				<Link to=''>
					<img src={ImgBanner} /> 
					<h3>
						ASSISTA AO VIVO E REVEJA AS 
						LUTAS QUANDO QUISER.<br/>
						EXCLUSIVO PARA ASSINANTES.
					</h3>
					<p>
						CONHEÇA >>
					</p>
				</Link>
			</div>
		);
	}
}

export default SliderMain;