import React from 'react';

class ArticleHome extends React.Component{
	render(){
		return(
			<article>
				<h2>
					A MAIOR COBERTURA DO UFC
				</h2>
				
				<p>
					O combate exibe <strong>ao vivo e na íntegra todas as edições do UFC </strong>
					com exclusividade, além de uma seleção com mais de 1.000 grandes desafios de 
					MMA. E o que é melhor: quando e onde você quiser, sem custo adicional.
				</p>
			</article>
		);
	}
}

export default ArticleHome;