import React, { Fragment } from 'react';
import {Switch, Route} from 'react-router-dom';

import './css/Style.scss';

import Home from './Home';

class Main extends React.Component{
	render(){
		return(
			<Fragment>
				<Route exact path='/' component={Home} />
				<Route path='/Home' component={Home} />
			</Fragment>
		);
	}
}

export default Main;