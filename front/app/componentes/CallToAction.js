import React from 'react';

import { Link } from 'react-router-dom';

class CallToAction extends React.Component{
	render(){
		return(
			<Link to='Assinatura' className="call-to-action">ASSINE JÁ</Link>
		);
	}
}

export default CallToAction;