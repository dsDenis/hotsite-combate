import React from 'react';

import { Link } from 'react-router-dom';
import Slider from 'react-slick';

import nocaute from './imagens/nocaute.png';
import meister from './imagens/meister-mma.jpg';
import pennington from './imagens/pennington.jpg';
import mma from './imagens/mma.jpg';

class SliderMain extends React.Component{
	render(){
		const settings = {
			accessibility: true,
			autoplay: false,
			dots: true,
			infinite: true,
			arrows: false,
			speed: 500,
			centerMode: true,
			fade: true,
			lazyLoad: true,
			pauseOnHover: true,
			touchMove: true,
			adaptiveHeight: true
		};
	
		return(
			<div className="container-slider">
				<Slider {...settings}>
					<div>
						<h3>ASSINE O COMBATE</h3><br/>
						<h4>LUTAS AO VIVO</h4><br/>
						<p>VEJA TODAS AS LUTAS DO UFC E O MELHOR DO MMA</p>
						<Link to='Assinatura' className="call-to-action">ASSINE JÁ</Link>
						<img src={nocaute} /> 
					</div>
					<div>
						<h3>ASSINE O COMBATE</h3><br/>
						<h4>LUTAS AO VIVO</h4><br/>
						<p>VEJA TODAS AS LUTAS DO UFC E O MELHOR DO MMA</p>
						<Link to='Assinatura' className="call-to-action">ASSINE JÁ</Link>
						<img src={meister} />
					</div>
					<div>
						<h3>ASSINE O COMBATE</h3><br/>
						<h4>LUTAS AO VIVO</h4><br/>
						<p>VEJA TODAS AS LUTAS DO UFC E O MELHOR DO MMA</p>
						<Link to='Assinatura' className="call-to-action">ASSINE JÁ</Link>
						<img src={pennington} />
					</div>
					<div>
						<h3>ASSINE O COMBATE</h3><br/>
						<h4>LUTAS AO VIVO</h4><br/>
						<p>VEJA TODAS AS LUTAS DO UFC E O MELHOR DO MMA</p>
						<Link to='Assinatura' className="call-to-action">ASSINE JÁ</Link>
						<img src={mma} />
					</div>
				</Slider>
			</div>
		);
	}
}

export default SliderMain;