import React from 'react';

import { Link } from 'react-router-dom';

class Parceiros extends React.Component{
	render(){
		return(
			<section className="parceiros">
				<h4>Escolha sua operadora e não perca nenhuma luta!</h4>
				<div className="operadoras-parceiras">
					<Link to="https://www.algartelecom.com.br" target="_blank">Algar Telecom</Link>
					<Link to="https://www.netcombo.com.br/static/html/netblue/" target="_blank">Blue</Link>
					<Link to="http://clarotv.claro.com.br/assineclarohdtv1" target="_blank">Claro</Link>
					<Link to="https://assine.vivo.com.br/gvt" target="_blank">GVT</Link>
					<Link to="https://www.netcombo.com.br/" target="_blank">NET</Link>
					<Link to="http://www.oi.com.br/tv-hd/" target="_blank">Oi TV</Link>
					<Link to="http://www.tvsky.com.br/assinante-sky/" target="_blank">SKY</Link>
					<Link to="https://assine.vivo.com.br/" target="_blank">VIVO</Link>
				</div>
			</section>
		);
	}
}

export default Parceiros;