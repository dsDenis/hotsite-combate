import React, { Fragment } from 'react';	

import ArticleHome from './ArticleHome';
import BoxEvent from './BoxEvent';
import CallToAction from './CallToAction';
import SliderMain from './SliderMain';
import Banner from './Banner';
import Parceiros from './Parceiros';

class Home extends React.Component{
	
	render(){
	
		return(
			<Fragment>
				<SliderMain />
				<ArticleHome />
				<BoxEvent />
				<CallToAction />
				<Banner />
				<Parceiros />
			</Fragment>
		);
	}
}

export default Home;